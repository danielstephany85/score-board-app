import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Header from './Header';
import Player from './Player';
import PlayerForm from './PlayerForm';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { players: this.props.player_data };
    this.nextID = this.state.players.length;
    this.onScoreChange = this.onScoreChange.bind(this);
  }


  onScoreChange(delta, i){
    this.state.players[i].score += delta;
    this.setState(this.state);
  }

  onPlayerAdd = (name) => {
    this.state.players.push({
      name: name,
      score: 0,
      id: this.nextID
    });
    this.setState(this.state);
    this.nextID++;
  }

  removePlayer = (index) => {
    this.state.players.splice(index, 1);
    this.setState(this.state);
  }

  render() {
    return (
      <div className="scoreboard">
        <Header title={this.props.title} players={this.props.player_data} />
        <div className="players">
          {
            this.state.players.map((player, i) => {
              return (
                <Player 
                  key={player.id} 
                  score={player.score} 
                  name={player.name} 
                  // the anonymous function must be bound to the class instance
                  // onScoreChange={function(delta){this.onScoreChange(delta, i)}.bind(this)}
                  // or an arrow function can be used to avoid context assignment 
                  onScoreChange={(delta) => { this.onScoreChange(delta, i) }}
                  onRemove={()=>{this.removePlayer(i)}}
                />
              );
            })
          }
        </div>
        <PlayerForm onAdd={this.onPlayerAdd}/>
      </div>
    );
  }
}

App.propTypes = {
  title: PropTypes.string,
  player_data: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    score: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired
  })).isRequired
}

Player.defaultProps = {
  title: "Score Board"
}

export default App;
