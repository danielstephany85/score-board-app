import React from 'react';
import PropTypes from 'prop-types';

function Stats(props){
    var totalPlayers = props.players.length;
    var totalScores = props.players.reduce((total, player) =>{
        return total + player.score;
    }, 0);

    return (
        <table className="stats">
            <tbody>
                <tr>
                    <td>Players:</td>
                    <td>{totalPlayers}</td>
                </tr>
                <tr>
                    <td>Total points:</td>
                    <td>{totalScores}</td>
                </tr>
            </tbody>
        </table>  
    );
}

export default Stats;

Stats.propTypes = {
    players: PropTypes.array.isRequired,
}

