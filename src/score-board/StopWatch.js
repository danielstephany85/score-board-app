import React, {Component} from 'react';
// import PropTypes from 'prop-types';

class StopWatch extends Component {
    constructor(props){
        super(props);
        this.state = {
            running: false,
            elapsedTime: 0,
            previousTime: 0
        }
    }

    // componentDidMount(){
       
    // }

    componentWillUnmount = () => {
        clearInterval(this.tickInterval);
    }

    onTick = () => {
        if(this.state.running){
            var now = Date.now();
            this.setState({
                running: true,
                previousTime: now,
                elapsedTime: this.state.elapsedTime + (now - this.state.previousTime)
            });
        }
    }

    onStart = () => {
        this.setState({ 
            running: true, 
            previousTime: Date.now()
        });
        this.tickInterval = setInterval(() => {
            this.onTick();
        }, 100);
    }

    onStop = () => {
        this.setState({ running: false });
        clearInterval(this.tickInterval);
    }

    onReset = () => {
        this.setState({ 
            elapsedTime: 0,
            previousTime: Date.now()
         });
    }

    render = () => {
        var seconds = Math.floor(this.state.elapsedTime / 1000);
        return (
            <div className="stopwatch">
                <h2>StopWatch</h2>
                <div className="stopwatch-time">{seconds}</div>
                {/* if true the first else the second */}
                {this.state.running ? <button onClick={ this.onStop}>Stop</button> : <button onClick={this.onStart}>Start</button>}
                <button onClick={this.onReset}>Reset</button>
            </div>
        );
    }
}



export default StopWatch;