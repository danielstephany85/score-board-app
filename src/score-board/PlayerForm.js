import React, {Component} from 'react';
import PropTypes from 'prop-types';

class PlayerForm extends Component {
    constructor(props){
        super(props);
        this.state = {name: ''}
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.props.onAdd(this.state.name);
        this.setState({name: ''});
    }

    onNameChange = (e) => {
        this.setState({ name: e.target.value })
    }

    render(){
        return (
            <div className="add-player-form">
                <form onSubmit={this.onSubmit}>
                    <input type='text'value={this.state.name} onChange={this.onNameChange}/>
                    <input type='submit'/>
                </form>
            </div>
        );
    }
}

PlayerForm.propTypes = {
    onAdd: PropTypes.func.isRequired
}

export default PlayerForm;