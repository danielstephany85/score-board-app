import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './score-board/App';

var players_data = [
    {
        name: "Daniel",
        score: 33,
        id: 0
    },
    {
        name: "Matt",
        score: 24,
        id: 1
    },
    {
        name: "kelly",
        score: 34,
        id: 2
    },
    {
        name: "amanda",
        score: 55,
        id: 3
    }
];

ReactDOM.render(<App player_data={players_data}/>,document.getElementById("root")); 